This program implements a basic binary search tree (bst) in C++.

Modify test.cpp to test inserting and finding in the bst. 

Then simply run ./test after running make with the makefile to compile.

You can also after running make run ./main followed by a list of names in a .txt file
to simulate constructing a bst from some input.
