/*
 * Name: Luke Tribble
 * Date: 10-14-17
 * Description: Implements functionality
 * to find certain nodes and do traversals.
 * Programming Assignment 1
 */
#ifndef BSTNODE_HPP
#define BSTNODE_HPP
#include <iostream>
#include <iomanip>

/** Starter code for PA1, CSE 100 2017
 * This code is based on code by
 * Christine Alvarado and Paul Kube.
 * Luke Tribble
 */

template<typename Data>
class BSTNode {

public:

  BSTNode<Data>* left;
  BSTNode<Data>* right;
  BSTNode<Data>* parent;
  Data const data;   // the const Data in this node.

  /** Constructor.  Initialize a BSTNode with the given Data item,
   *  no parent, and no children.
   */
  BSTNode(const Data & d);


  /** Return the successor of this BSTNode in a BST, or 0 if none.
   ** PRECONDITION: this BSTNode is a node in a BST.
   ** POSTCONDITION:  the BST is unchanged.
   ** RETURNS: the BSTNode that is the successor of this BSTNode,
   ** or 0 if there is none.
   */
  BSTNode<Data>* successor();
  
  /* Function finds the parent's successor for the corresponding node.
   */ 
  BSTNode<Data>* inorderparent();
  /* Function finds the leftmost node of the corresponding node.
   */ 
  BSTNode<Data>* getleft(); 

}; 


/*Function definitions
* For a templated class it's easiest to just put them in the same
* file as the class declaration
*/
template <typename Data>
BSTNode<Data>::BSTNode(const Data & d) : left(0), right(0),
 parent(0), data(d) {}

/* Function finds the parent's successor for the corresponding node.
 */ 
template <typename Data> BSTNode<Data>* BSTNode<Data>::inorderparent()
{
  if(nullptr == parent) //If the parent is null, exit.
  {
    return 0;
  }
  else if(parent->right == this) //If the node is the parent's right child..
  {
    return parent->inorderparent(); //Find the parent's successor...
  }
  else
  {
    return parent; //Otherwise just return the parent.
  }
}

/* Function finds the leftmost node of the corresponding node.
 */ 
template <typename Data> BSTNode<Data>* BSTNode<Data>::getleft()
{
  if(nullptr == left) //If the leftmost node is null, just return this node.
  {
    return this;
  }
  else
  {
    return left->getleft(); //Otherwise, descend down the left side.
  }
}



/* Return a pointer to the BSTNode that contains the 
 * item that is sequentially next 
 * in the tree */
template <typename Data>
BSTNode<Data>* BSTNode<Data>::successor()
{
  if(nullptr != right) //If there is a right child get it's left child.
  {
    return right->getleft();
  }
  else if(nullptr == parent) //Exit if the parent is null.
  {
    return 0;
  }
  else if(parent->left == this) //If the left child of the parent is called...
  {
    return parent; //The parent is the successor.
  }
  else
  {
    return parent->inorderparent(); //Other wise get the parent's successor.
  }
}

/** Overload operator<< to print a BSTNode's fields to an ostream. */
template <typename Data>
std::ostream & operator<<(std::ostream& stm, const BSTNode<Data> & n) {
  stm << '[';
  stm << std::setw(10) << &n;                 // address of the BSTNode
  stm << "; p:" << std::setw(10) << n.parent; // address of its parent
  stm << "; l:" << std::setw(10) << n.left;   // address of its left child
  stm << "; r:" << std::setw(10) << n.right;  // address of its right child
  stm << "; d:" << n.data;                    // its data field
  stm << ']';
  return stm;
}

#endif // BSTNODE_HPP
