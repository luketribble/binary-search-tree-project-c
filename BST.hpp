/*
 * Name: Luke Tribble
 * Date: 10-14-17
 * Description: Implements the main functionality of the BST.
 * Programming Assignment 1
 */
#ifndef BST_HPP
#define BST_HPP
#include "BSTNode.hpp"
#include "BSTIterator.hpp"
#include <iostream>

template<typename Data>
class BST {

public:

  /** define iterator as an aliased typename for BSTIterator<Data>. */
  typedef BSTIterator<Data> iterator;

  /** Default constructor.
      Initialize an empty BST.
      This is inlined because it is trivial.
   */
  BST() : root(nullptr), isize(0) {}


  /** Default destructor.
      Delete every node in this BST.
  */
  ~BST();

  /** Given a reference to a Data item, insert a copy of it in this BST.
   *  Return a pair where the first element is an iterator 
   *  pointing to either the newly inserted element or the element 
   *  that was already in the BST, and the second element is true if the 
   *  element was newly inserted or false if it was already in the BST.
   * 
   *  Note: This function should use only the '<' operator when comparing
   *  Data items. (should not use ==, >, <=, >=)  
   */
  std::pair<iterator, bool> insert(const Data& item);


  /** Find a Data item in the BST.
   *  Return an iterator pointing to the item, or pointing past
   *  the last node in the BST if not found.
   *  Note: This function should use only the '<' operator when comparing
   *  Data items. (should not use ==, >, <=, >=).  For the reasoning
   *  behind this, see the assignment writeup.
   */
  iterator find(const Data& item) const;

  
  /** Return the number of items currently in the BST.
   */ 
  unsigned int size() const;
  
  /** Return the height of the BST.
      height of a tree starts with root at height 0
   */ 
  unsigned int height() const;


  /** Return true if the BST is empty, else false.
   */ 
  bool empty() const;

  /** Return an iterator pointing to the first (smallest) item in the BST.
   */ 
  iterator begin() const;

  /** Return an iterator pointing past the last item in the BST.
   */
  iterator end() const;

  /** Helper method that takes in a BSTNode to recursively find
   *  the height from that node.
   */ 
  unsigned int heighthelp(BSTNode<Data> * n) const;

private:

  /** Pointer to the root of this BST, or 0 if the BST is empty */
  BSTNode<Data>* root;
  
  /** Number of Data items stored in this BST. */
  unsigned int isize;

  /** Find the first element of the BST
   * Helper function for the begin method above.
   */ 
  static BSTNode<Data>* first(BSTNode<Data>* root);

  /** do a postorder traversal, deleting nodes
   */
  static void deleteAll(BSTNode<Data>* n);

  

  

 };


// ********** Function definitions ****************


/** Default destructor.
    Delete every node in this BST.
*/

template <typename Data>
BST<Data>::~BST() {
  deleteAll(root);
}

template <typename Data>
unsigned int BST<Data>::heighthelp(BSTNode<Data> * n) const
{
  if(nullptr == n)
  {
    return -1; //Account for height offset for an empty tree.
  }
  //Recursively finds the height of each subtree.
  int leftsub = heighthelp(n->left);
  int rightsub = heighthelp(n->right);

  if(rightsub >= leftsub)
  {
    return rightsub + 1; //Return greater height to find full height of tree.
  } 
  else
  {
    return leftsub + 1;
  }   
}


/** Given a reference to a Data item, insert a copy of it in this BST.
 *  Return a pair where the first element is an iterator pointing to 
 *  either the newly inserted
 *  element or the element that was already in the BST, and the second element 
 *  is true if the 
 *  element was newly inserted or false if it was already in the BST.
 * 
 *  Note: This function should use only the '<' operator when comparing
 *  Data items. (should not use ==, >, <=, >=)  
 */
template <typename Data>
std::pair<BSTIterator<Data>, bool> BST<Data>::insert(const Data& item) {

  if(root) //If the root already exists...
  {
    BSTNode<Data> * selected = root; //Make our current node the root
    while(selected) //Keep searching through the tree
    {
      if(item < selected->data) //If insert is less than the node in question..
      {
        if(nullptr != selected->left) //If a node is present, descend down
        {
          selected = selected->left;
        }
        else //Otherwise, make a new node, update parent and increase size.
        {
          selected->left = new BSTNode<Data>(item);
          selected->left->parent = selected;
          isize = isize + 1;
          return std::pair<BSTIterator<Data>,bool>
          (BSTIterator<Data>(selected->left), true);
        }
      }
      else if(selected->data < item) //Use same recursive logic for the right
      {
        if(nullptr != selected->right)
        {
          selected = selected->right;
        }
        else
        {
          selected->right = new BSTNode<Data>(item);
          selected->right->parent = selected;
          isize = isize +1;
          return std::pair<BSTIterator<Data>,bool>
          (BSTIterator<Data>(selected->right),true);
        }
      }
      else //Return false if a duplicate value is detected.
      {
        return std::pair<BSTIterator<Data>,bool>
        (BSTIterator<Data>(selected),false);
      }
    } //This line below was used just to fix a compilation error.
    return std::pair<BSTIterator<Data>,bool>(BSTIterator<Data>(root),false);
  }
  else //Otherwise, if the root does not exist, create one and update the size.
  {
    root = new BSTNode<Data>(item);
    isize = isize + 1;
    return std::pair<BSTIterator<Data>,bool>(BSTIterator<Data>(root),true);
  }
  
}


/** Find a Data item in the BST.
 *  Return an iterator pointing to the item, or pointing past
 *  the last node in the BST if not found.
 *  Note: This function should use only the '<' operator when comparing
 *  Data items. (should not use ==, >, <=, >=).  For the reasoning
 *  behind this, see the assignment writeup.
 */
template <typename Data>
BSTIterator<Data> BST<Data>::find(const Data& item) const
{
  BSTNode<Data> * selected = root;

  while(selected) //If the value is smaller than the node, descend left.
  {
    if ( item < selected->data)
    {
      selected = selected->left;
    }
    else if(selected->data < item) //Otherwise, descend right.
    {
      selected = selected->right;
    }
    else //Return the found node.
    {
      return BSTIterator<Data>(selected);
    }
  }
  return end(); //Return past the iterator if null node detected.
}

  
/** Return the number of items currently in the BST.
 */ 
template <typename Data>
unsigned int BST<Data>::size() const
{
  return isize;
}

/** Return the height of the BST.
 */
template <typename Data> 
unsigned int BST<Data>::height() const
{
  return heighthelp(root); //Call recursive helper method to find height.
}


/** Return true if the BST is empty, else false.
 */ 
template <typename Data>
bool BST<Data>::empty() const
{
  return isize == 0; 
}

/** Return an iterator pointing to the first (smallest) item in the BST.
 */ 
template <typename Data>
BSTIterator<Data> BST<Data>::begin() const
{
  return BSTIterator<Data>(first(root));
}

/** Return an iterator pointing past the last item in the BST.
 */
template <typename Data>
BSTIterator<Data> BST<Data>::end() const
{
  return BSTIterator<Data>(nullptr);
}

/** Find the first element of the BST
 * Helper function for the begin method above.
 */ 
template <typename Data>
BSTNode<Data>* BST<Data>::first(BSTNode<Data>* root)
{ //Handle a null exception.
  if(nullptr == root)
  {
    return 0;
  } 
  while(nullptr != root->left)
  { //Keep descending left to find the smallest data node.
    root = root->left;
  }
  return root;
}

/** do a postorder traversal, deleting nodes
 */
template <typename Data>
void BST<Data>::deleteAll(BSTNode<Data>* n)
{
  if ( 0 == n)
  {
    return;
  } //Delete the node and all of its children.
  deleteAll(n->right);
  deleteAll(n->left);
  delete n;

}



#endif //BST_HPP
